package saucebot

import (
	"github.com/ddliu/motto"
	irc "github.com/fluffle/goirc/client"
	"github.com/robertkrimen/otto"
)

// IRCBridge -
type IRCBridge struct {
	conn *irc.Conn
	ob   *otto.Object
}

// NewIRCBridge -
func NewIRCBridge(conn *irc.Conn) *IRCBridge {
	br := new(IRCBridge)
	br.conn = conn
	return br
}

func (br *IRCBridge) setupObject(vm *motto.Motto) (otto.Value, error) {
	conv, err := vm.Otto.ToValue(br.conn)
	if err != nil {
		panic(err)
	}
	br.ob = conv.Object()

	br.ob.Set(irc.REGISTER, irc.REGISTER)
	br.ob.Set(irc.CONNECTED, irc.CONNECTED)
	br.ob.Set(irc.DISCONNECTED, irc.DISCONNECTED)
	br.ob.Set(irc.ACTION, irc.ACTION)
	br.ob.Set(irc.AWAY, irc.AWAY)
	br.ob.Set(irc.CAP, irc.CAP)
	br.ob.Set(irc.CTCP, irc.CTCP)
	br.ob.Set(irc.CTCPREPLY, irc.CTCPREPLY)
	br.ob.Set(irc.INVITE, irc.INVITE)
	br.ob.Set(irc.JOIN, irc.JOIN)
	br.ob.Set(irc.KICK, irc.KICK)
	br.ob.Set(irc.MODE, irc.MODE)
	br.ob.Set(irc.NICK, irc.NICK)
	br.ob.Set(irc.NOTICE, irc.NOTICE)
	br.ob.Set(irc.OPER, irc.OPER)
	br.ob.Set(irc.PART, irc.PART)
	br.ob.Set(irc.PASS, irc.PASS)
	br.ob.Set(irc.PING, irc.PING)
	br.ob.Set(irc.PONG, irc.PONG)
	br.ob.Set(irc.PRIVMSG, irc.PRIVMSG)
	br.ob.Set(irc.QUIT, irc.QUIT)
	br.ob.Set(irc.TOPIC, irc.TOPIC)
	br.ob.Set(irc.USER, irc.USER)
	br.ob.Set(irc.VERSION, irc.VERSION)
	br.ob.Set(irc.VHOST, irc.VHOST)
	br.ob.Set(irc.WHO, irc.WHO)
	br.ob.Set(irc.WHOIS, irc.WHOIS)

	return vm.ToValue(br.ob)
}
