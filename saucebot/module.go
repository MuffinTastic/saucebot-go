package saucebot

import (
	"errors"
	"log"
	"sync"

	"github.com/ddliu/motto"
	"github.com/robertkrimen/otto"
)

type importType interface {
	moduleImportType()
}

type importObj struct {
	name   string
	object *otto.Object
}

type importFunc struct {
	name   string
	object motto.ModuleLoader
}

func (x importObj) moduleImportType()  {}
func (x importFunc) moduleImportType() {}

// Module is a dynamically loaded object that contains IRC-callable JavaScript functions.
type Module struct {
	file string
	name string
	desc string

	sync.Mutex
	commands map[string]*otto.Value
	events   map[string][]*otto.Value

	imports []importType
	vm      *motto.Motto
	ob      *otto.Object

	modsys *ModuleSystem
}

// NewModule creates a new Module object :S
func NewModule(file string, modsys *ModuleSystem) *Module {
	m := new(Module)
	m.file = file
	m.modsys = modsys
	return m
}

// AddImportObject needs a good comment above it
func (m *Module) AddImportObject(name string, mod *otto.Object) {
	m.imports = append(m.imports, importObj{name, mod})
}

// AddImportFunction needs a good comment above it
func (m *Module) AddImportFunction(name string, mod motto.ModuleLoader) {
	m.imports = append(m.imports, importFunc{name, mod})
}

// Reload reloads.......
func (m *Module) Reload() bool {
	m.vm = motto.New()
	m.commands = make(map[string]*otto.Value)
	m.events = make(map[string][]*otto.Value)
	m.name = m.file
	m.desc = "(no description)"

	moduleloader := func(vm *motto.Motto) (otto.Value, error) {
		m.ob, _ = m.vm.Object("({})")
		m.ob.Set("Name", m.getName)
		m.ob.Set("Description", m.getDesc)
		m.ob.Set("SetName", m.setName)
		m.ob.Set("SetDesc", m.setDesc)

		m.ob.Set("CommandList", m.CommandList)
		m.ob.Set("Reload", m.Reload)

		m.ob.Set("AddCommand", m.addCommand)
		m.ob.Set("AddEvent", m.addEvent)
		m.ob.Set("CallEvent", m.callEvent)

		m.ob.Set("log", m.consoleLog)
		return vm.ToValue(m.ob)
	}

	m.vm.AddModule("module", moduleloader)
	for _, v := range m.imports {
		switch obj := v.(type) {
		case importObj:
			m.vm.AddModule(obj.name, func(vm *motto.Motto) (otto.Value, error) {
				return vm.ToValue(obj)
			})
		case importFunc:
			m.vm.AddModule(obj.name, obj.object)
		}
	}

	_, err := m.vm.Run(m.file)
	if err != nil {
		log.Printf("[MODULE %s] [ERROR] %s\n", m.name, err.Error())
		return false
	}

	log.Printf("[MODULE %s] [INFO] Loaded!\n", m.name)
	return true
}

var errNotFunc = errors.New("module.AddCommand: Second argument not function()")

func (m *Module) addCommand(call otto.FunctionCall) otto.Value {

	name := call.Argument(0).String()
	funct := call.Argument(1)
	if !funct.IsFunction() {
		log.Printf("[MODULE %s] [ERROR] %s\n", m.name, errNotFunc.Error())
		return otto.UndefinedValue()
	}
	if m.modsys.CommandExists(name) {
		log.Printf(`[MODULE %s] [ERROR] module.AddCommand: Command with name "%s" already exists`, m.name, name)
		return otto.UndefinedValue()
	}
	m.Lock()
	m.commands[name] = &funct
	m.Unlock()
	return otto.UndefinedValue()
}

func (m *Module) addEvent(call otto.FunctionCall) otto.Value {

	name := call.Argument(0).String()
	funct := call.Argument(1)
	if !funct.IsFunction() {
		panic(errNotFunc)
	}
	m.Lock()
	m.events[name] = append(m.events[name], &funct)
	m.Unlock()
	return otto.UndefinedValue()
}

func (m *Module) getName() string {
	return m.name
}

func (m *Module) getDesc() string {
	return m.desc
}

func (m *Module) setName(name string) {
	m.name = name
}

func (m *Module) setDesc(desc string) {
	m.desc = desc
}

func (m *Module) consoleLog(text string) {
	log.Printf(`[MODULE %s] [LOG] %s`, m.name, text)
}

// CommandList -
func (m *Module) CommandList() []string {
	var funcNames []string

	m.Lock()
	for k := range m.commands {
		funcNames = append(funcNames, k)
	}
	m.Unlock()

	return funcNames
}

func (m *Module) callEvent(lcall otto.FunctionCall) otto.Value {
	name := lcall.Argument(0).String()
	var event *otto.Object
	if len(lcall.ArgumentList) > 1 {
		event = lcall.Argument(1).Object()
	}

	m.Lock()
	if _, ok := m.events[name]; ok {
		funcs := m.events[name]
		for i := 0; i < len(funcs); i++ {
			go call(funcs[i], event, m)
		}
	}
	m.Unlock()

	return otto.UndefinedValue()
}

// CommandExists is a simple wrapper for _,ok:=map.
func (m *Module) CommandExists(name string) bool {
	m.Lock()
	_, ok := m.commands[name]
	m.Unlock()
	return ok
}

// GetObject -
func (m *Module) GetObject() *otto.Object {
	return m.ob
}

func call(funct *otto.Value, event *otto.Object, m *Module) { // this sucks
	defer func() { // screw u and ur panics !!!
		if r := recover(); r != nil {
			log.Printf("[MODULE %s] [ERROR] %s\n", m.name, r)
		}
	}()

	var err error
	if event == nil {
		_, err = funct.Call(otto.UndefinedValue())
	} else {
		_, err = funct.Call(otto.UndefinedValue(), event)
	}
	if err != nil {
		log.Printf("[MODULE %s] [ERROR] %s\n", m.name, err)
	}
}

// CallCommand calls commands if they exist
func (m *Module) CallCommand(name string, event *IRCEvent) {
	m.Lock()
	if _, exists := m.commands[name]; exists {
		go call(m.commands[name], event.createObject(m.vm), m)
	}
	m.Unlock()
}

// CallEvent calls all events of a certain name
func (m *Module) CallEvent(name string, event *IRCEvent) {
	m.Lock()
	if _, ok := m.events[name]; ok {
		funcs := m.events[name]
		for i := 0; i < len(funcs); i++ {
			go call(funcs[i], event.createObject(m.vm), m)
		}
	}
	m.Unlock()
}
