package saucebot

import (
	"encoding/json"
	"io/ioutil"

	"github.com/ddliu/motto"
	"github.com/robertkrimen/otto"
)

type data struct {
	Nickname    string `json:"nick"`
	Description string `json:"desc"`
	Prefix      string `json:"prefix"`

	Server   string `json:"server"`
	Password string `json:"password"`
	SSL      bool   `json:"ssl"`

	Custom   map[string]string `json:"custom"`
	Channels map[string]string `json:"channels"`
	Modules  []string          `json:"modules"`
}

// Config -
type Config struct {
	file string

	data
}

// NewConfig -
func NewConfig(file string) *Config {
	c := new(Config)
	c.file = file
	c.Reload()
	return c
}

func (c *Config) setupObject(vm *motto.Motto) (otto.Value, error) {
	ob, _ := vm.Object("({})")
	ob.Set("nickname", c.Nickname)
	ob.Set("description", c.Description)
	ob.Set("prefix", c.Prefix)

	ob.Set("server", c.Server)
	ob.Set("ssl", c.SSL)

	ob.Set("custom", c.Custom)
	ob.Set("channels", c.getChans())
	ob.Set("modules", c.Modules)
	return vm.ToValue(ob)
}

// Reload -
func (c *Config) Reload() {
	byt, err := ioutil.ReadFile(c.file)
	if err != nil {
		panic(err)
	}
	if err = json.Unmarshal(byt, &c.data); err != nil {
		panic(err)
	}
}

func (c *Config) getChans() []string {
	var chans []string
	for ch := range c.Channels {
		chans = append(chans, ch)
	}
	return chans
}
