package saucebot

import (
	"fmt"
	"strconv"

	"github.com/ddliu/motto"
	"github.com/robertkrimen/otto"
)

// Formatting wrapper for ease of use :)
type Formatting struct {
}

const formatASCIIUnderscore = rune(0x1F)
const formatASCIIItalics = rune(0x1D)
const formatASCIIReset = rune(0x0F)
const formatASCIIColor = rune(0x03)
const formatASCIIBold = rune(0x02)

const (
	formatTextUnderscore int = 1 << iota
	formatTextItalics
	formatTextBold
)

// http://www.mirc.com/colors.html
const formatColorMin = 0
const formatColorMax = 15
const (
	formatColorWhite int = iota
	formatColorBlack
	formatColorBlue
	formatColorGreen
	formatColorLightRed
	formatColorBrown
	formatColorPurple
	formatColorOrange
	formatColorYellow
	formatColorLightGreen
	formatColorCyan
	formatColorLightCyan
	formatColorLightBlue
	formatColorPink
	formatColorGrey
	formatColorLightGrey
)

func (f *Formatting) setupObject(vm *motto.Motto) (otto.Value, error) {
	ob, _ := vm.Object("({})")

	asc, _ := vm.Object("({})")

	asc.Set("Underscore", formatASCIIUnderscore)
	asc.Set("Italics", formatASCIIItalics)
	asc.Set("Reset", formatASCIIReset)
	asc.Set("Color", formatASCIIColor)
	asc.Set("Bold", formatASCIIBold)

	ob.Set("ASCII", asc)

	txt, _ := vm.Object("({})")

	txt.Set("Underscore", formatTextUnderscore)
	txt.Set("Italics", formatTextItalics)
	txt.Set("Bold", formatTextBold)

	ob.Set("Text", txt)

	cols, _ := vm.Object("({})")

	cols.Set("White", formatColorWhite)
	cols.Set("Black", formatColorBlack)
	cols.Set("Blue", formatColorBlue)
	cols.Set("Green", formatColorGreen)
	cols.Set("LightRed", formatColorLightRed)
	cols.Set("Brown", formatColorBrown)
	cols.Set("Purple", formatColorPurple)
	cols.Set("Orange", formatColorOrange)
	cols.Set("Yellow", formatColorYellow)
	cols.Set("LightGreen", formatColorLightGreen)
	cols.Set("Cyan", formatColorCyan)
	cols.Set("LightCyan", formatColorLightCyan)
	cols.Set("LightBlue", formatColorLightBlue)
	cols.Set("Pink", formatColorPink)
	cols.Set("Grey", formatColorGrey)
	cols.Set("LightGrey", formatColorLightGrey)
	cols.Set("None", -1)

	ob.Set("Color", cols)

	ob.Set("FormatText", f.formatText)
	ob.Set("ColorText", f.colorText)

	return vm.ToValue(ob)
}

func (f *Formatting) formatText(text string, flags int) string {
	result := text

	if flags&formatTextUnderscore != 0 {
		result = fmt.Sprintf("%c%s%c", formatASCIIUnderscore, result, formatASCIIUnderscore)
	}

	if flags&formatTextItalics != 0 {
		result = fmt.Sprintf("%c%s%c", formatASCIIItalics, result, formatASCIIItalics)
	}

	if flags&formatTextBold != 0 {
		result = fmt.Sprintf("%c%s%c", formatASCIIBold, result, formatASCIIBold)
	}

	return result
}

func (f *Formatting) colorText(text string, foreground int, background int) string {
	if foreground == -1 {
		return text
	}

	var header string

	if background > -1 {
		header = fmt.Sprintf("%s,%s", strconv.Itoa(foreground), strconv.Itoa(background))
	} else {
		header = strconv.Itoa(foreground)
	}

	return fmt.Sprintf("%c%s%s%c", formatASCIIColor, header, text, formatASCIIColor)
}
