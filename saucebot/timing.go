package saucebot

import (
	"time"

	"github.com/ddliu/motto"
	"github.com/robertkrimen/otto"
)

// Timing -
type Timing struct {
	queue []*otto.Value
}

func (t *Timing) setupObject(vm *motto.Motto) (otto.Value, error) {
	ob, _ := vm.Object("({})")

	ob.Set("Nanosecond", time.Nanosecond)
	ob.Set("Microsecond", time.Microsecond)
	ob.Set("Millisecond", time.Millisecond)
	ob.Set("Second", time.Second)
	ob.Set("Minute", time.Minute)

	ob.Set("Sleep", t.sleep)

	return vm.ToValue(ob)
}

func (t *Timing) sleep(call otto.FunctionCall) otto.Value {
	dl, err := call.Argument(0).ToInteger()
	if err != nil {
		return otto.TrueValue()
	}
	time.Sleep(time.Duration(dl))
	return otto.UndefinedValue()
}
