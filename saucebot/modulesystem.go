package saucebot

import (
	"errors"
	"log"
	"os"
	"runtime"

	"github.com/ddliu/motto"
	"github.com/robertkrimen/otto"
)

// ModuleSystem -
type ModuleSystem struct {
	modules    []*Module
	config     *Config
	bridge     *IRCBridge
	timing     *Timing
	formatting *Formatting

	ob *otto.Object
}

// STARTUP is a custom event \:D/
const (
	STARTUP  = "STARTUP"
	SHUTDOWN = "SHUTDOWN"
)

// NewModuleSystem -
func NewModuleSystem(cfg *Config, irc *IRCBridge) *ModuleSystem {
	ms := new(ModuleSystem)
	ms.config = cfg
	ms.bridge = irc
	ms.timing = new(Timing)
	ms.formatting = new(Formatting)
	ms.Reset()
	ms.CallEvent(STARTUP, nil)
	runtime.SetFinalizer(ms, (*ModuleSystem).finalizer)
	return ms
}

func (ms *ModuleSystem) finalizer() {
	ms.CallEvent(SHUTDOWN, nil)
}

func (ms *ModuleSystem) setupObject(vm *motto.Motto) (otto.Value, error) {
	ms.ob, _ = vm.Object("({})")
	ms.ob.Set("Reload", ms.Reload)
	ms.ob.Set("Reset", ms.Reset)
	ms.ob.Set("Modules", ms.getModules)
	ms.ob.Set("CallEvent", ms.CallEvent)

	ms.ob.Set(STARTUP, STARTUP)
	ms.ob.Set(SHUTDOWN, SHUTDOWN)
	return vm.ToValue(ms.ob)
}

// Reload -
func (ms *ModuleSystem) Reload() {
	for _, module := range ms.modules {
		module.Reload()
	}
}

var errNotString = errors.New("ModuleSystem: non-string in module config")

// Reset -
func (ms *ModuleSystem) Reset() {
	ms.modules = nil
	ms.config.Reload()
	for _, file := range ms.config.Modules {
		if _, err := os.Stat(file); err != nil {
			log.Printf("[MODULE SYSTEM] [ERROR] Could not load module %s: %s\n", file, err.Error())
			continue
		}

		log.Printf("[MODULE SYSTEM] [INFO] Loading module %s...\n", file)

		mod := NewModule(file, ms)

		mod.AddImportFunction("config", ms.config.setupObject)
		mod.AddImportFunction("irc", ms.bridge.setupObject)
		mod.AddImportFunction("timing", ms.timing.setupObject)
		mod.AddImportFunction("formatting", ms.formatting.setupObject)
		mod.AddImportFunction("modulesystem", ms.setupObject)

		if mod.Reload() == true {
			ms.modules = append(ms.modules, mod)
		}
	}
}

// getModules -
func (ms *ModuleSystem) getModules(call otto.FunctionCall) otto.Value {
	modsObj, _ := call.Otto.Object("({})")

	for _, mod := range ms.modules {
		modVal, _ := call.Otto.ToValue(mod.GetObject())
		modsObj.Set(mod.name, modVal)
	}

	val, _ := call.Otto.ToValue(modsObj)
	return val
}

// CommandExists -
func (ms *ModuleSystem) CommandExists(name string) bool {
	for _, mod := range ms.modules {
		if mod.CommandExists(name) {
			return true
		}
	}
	return false
}

func (ms *ModuleSystem) callEvent(call otto.FunctionCall) otto.Value {
	for _, mod := range ms.modules {
		mod.callEvent(call)
	}
	return otto.UndefinedValue()
}

// CallEvent -
func (ms *ModuleSystem) CallEvent(name string, event *IRCEvent) {
	for _, mod := range ms.modules {
		mod.CallEvent(name, event)
	}
}

// CallCommand -
func (ms *ModuleSystem) CallCommand(name string, event *IRCEvent) {
	for _, mod := range ms.modules {
		mod.CallCommand(name, event)
	}
}
