package saucebot

import (
	"fmt"
	"strings"

	"github.com/ddliu/motto"
	irc "github.com/fluffle/goirc/client"
	"github.com/robertkrimen/otto"
)

// IRCEvent -
type IRCEvent struct {
	nick, ident, host, src string
	target                 string
	text                   string
	split, args            []string
	public, Local          bool
	IsCommand              bool
	Command                string
}

// NewEvent -
func NewEvent(line *irc.Line, conn *irc.Conn, prefix ...string) *IRCEvent {
	event := new(IRCEvent)

	event.nick = line.Nick
	event.ident = line.Ident
	event.host = line.Host
	event.src = line.Src

	event.target = line.Target()
	event.text = line.Text()
	event.split = strings.Split(event.text, " ")
	event.public = line.Public()
	event.Local = (event.nick == conn.Me().Nick)

	if len(prefix) > 0 {
		event.IsCommand = strings.HasPrefix(event.text, prefix[0])
	}

	if event.IsCommand {
		event.args = event.split[1:]
		event.text = strings.Join(event.args, " ")
		event.Command = event.split[0][len(prefix[0]):]
	} else {
		event.args = event.split
	}

	return event
}

func (ev *IRCEvent) createObject(vm *motto.Motto) *otto.Object {
	if ev != nil {
		ob, _ := vm.Object("({})")

		ob.Set("nick", ev.nick)
		ob.Set("ident", ev.ident)
		ob.Set("host", ev.host)
		ob.Set("src", ev.src)
		ob.Set("local", ev.Local)
		ob.Set("public", ev.public)
		ob.Set("target", ev.target)
		ob.Set("text", ev.text)
		ob.Set("split", ev.split)
		ob.Set("args", ev.args)

		ob.Set("Format", ev.format)

		return ob
	}

	return nil
}

func (ev *IRCEvent) format(call otto.FunctionCall) otto.Value {
	val, _ := call.Otto.ToValue(
		fmt.Sprintf("[%s] <%s> %s",
			ev.target,
			ev.nick,
			ev.text,
		),
	)
	return val
}
