var modsys = require("modulesystem")
var module = require("module");
var cfg = require("config")
var irc = require("irc")
var timing = require("timing")

module.SetName("QAuth")

// in config:
//  "custom": {
//      "qnet-auth": "account-here",
//      "qnet-pass": "password-here"
//  },

module.AddEvent(irc.REGISTER, function() {
    module.log("Attempting to authenticate with Q...")
    irc.Privmsgf("q@cserve.quakenet.org", "AUTH %s %s", cfg.custom["qnet-auth"], cfg.custom["qnet-pass"])
    timing.Sleep(5 * timing.Second)
    irc.Raw(irc.MODE + " " + cfg.nickname + " +x")
})
