// copyright muffin 2016 don't steal please :(

// Exposed API:

var modsys = require("modulesystem")
// modulesystem -- contains all modules :
//              Reset()   : dumps existing modules, reloads config, repopulates & loads modules from new module list
//              Reload()  : calls Reload() on all modules, which dumps the existing VM (if any exists) and loads script from the filename/filepath specified.
//     object[] Modules() : returns a javascript array of objects that represent the modules (object as in the "module" import)

var module = require("module")
// module -- current module :
//            AddCommand(string name, func) : adds named JS function to module's dictionary (aka "map") (see structure below)
//            AddEvent(string event, func)  : adds anonymous JS function to module's array of funcs (see structure below)
//            Reload()                      : reloads script just like if you called modsys.Reload(). not sure why you'd do this though
//     string Name()                        : returns the name / filepath (module name defaults to filepath if nothing is set)
//            SetName(string name)          : sets module's name. not terribly useful, but yknow :P
//     string Description()                 : returns the description (returns "(no description)" if nothing is set)
//            SetDesc(string description)   : sets module's description
//  str array CommandList()                 : list/array of module's commands
// NOTE: with a combination of modsys.Modules() and module.CommandList(), you could make a 'help' command :)

var irc    = require("irc")
// irc -- just look at http://www.github.com/fluffle/goirc/. i'm too lazy to list EVERYTHING here.
// (it's a Golang -> JavaScript conversion of goirc/client/connection.go's Conn struct.)

// config -- exposed values from JSON cfg file. :
//                string nickname    : nickname of the bot (might not be actual current nickname, use irc.Me.Nick for that!)
//                string description : description of the bot
//                string prefix      : prefix the bot is using for commands
//                string server      : "server:port" the bot is connected to
//                  bool ssl         : using SSL for the connection?
//       str -> str dict custom      : custom values for modules to use
//             str array channels    : list/array of channels. (might not be accurate, use dict irc.Me.Channels for that!)
//             str array modules     : list/array of modules   (might not be accurate -- some might have errored while loading!)
// NOTE: changing any of these will not change the config. might make it do that idk

// timing -- basic timing stuff, only supports Sleep() atm :
//     int64 Nanosecond      : Duration / measurement of time
//     int64 Microsecond     : ditto
//     int64 Millisecond     : ditto ditto
//     int64 Second          : ditto ditto ditto
//     int64 Minute          : ditto ditto ditto ditto
//           Sleep(Duration) : Pause execution in current 'thread' for this amount of time.
// NOTE: Sleep will work with float multiplication! (i.e. Sleep(1.47 * timing.Second))
// NOTE: Hour exists in golang's time package, but not here. who wants a command pausing for hours???
//       If you *really* want that, do it yourself lol

var formatting = require("formatting")
// formatting -- convenience "library"
//          dict ASCII: raw ascii characters for IRC formatting
//             rune Underscore
//             rune Italics
//             rune Reset
//             rune Color
//             rune Bold
//
//          dict Text: bitmask
//        (int) bit Underscore
//              bit Italics
//              Bit bold
//
//          dict Colors: IRC color codes
//              int White
//              int Black
//              int Blue
//              int Green
//              int LightRed
//              int Brown
//              int Purple
//              int Orange
//              int Yellow
//              int LightGreen
//              int Cyan
//              int LightCyan
//              int LightBlue
//              int Pink
//              int Grey
//              int LightGrey
//              int None
//
//   string FormatText(text string, bitmask int)
//   string ColorText(text string, foregroundColor int, backgroundColor int)
// NOTE: you can get rid of the background color if you set it to None

module.SetName("General")

module.AddCommand("reset", function(event) {
    if (event.host != "MuffinTastic32.users.quakenet.org") // MINE!! :)
        return

    modsys.Reset()
    irc.Privmsg(event.target, "Reset modules & config.")
})

module.AddCommand("reload", function(event) {
    if (event.host != "MuffinTastic32.users.quakenet.org")
        return

    modsys.Reload()
    irc.Privmsg(event.target, "Reloaded modules.")
})

module.AddCommand("eval", function(event) {
    if (event.host != "MuffinTastic32.users.quakenet.org")
        return

    if (event.args.length > 0) {
        command = event.text
        console.log("[!!! EVAL !!!]\n" + command)
        eval(command)
    }
})
