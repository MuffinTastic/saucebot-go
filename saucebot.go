package main

import (
	"flag"
	"log"
	"os"
	"os/signal"
	"runtime"
	"time"

	"bitbucket.org/MuffinTastic/saucebot-go/saucebot"

	irc "github.com/fluffle/goirc/client"
)

const saucebotVersion = "1.0"

var moduleSystem *saucebot.ModuleSystem
var ircBridge *saucebot.IRCBridge
var sauceCfg *saucebot.Config

func init() {
	var cfgFile string
	flag.StringVar(&cfgFile, "config", "saucebot.json", "Config file to use. Defaults to ./saucebot.json.")
	flag.Parse()
	sauceCfg = saucebot.NewConfig(cfgFile)
}

func main() {
	ircCfg := irc.NewConfig("")

	ircCfg.Server = sauceCfg.Server
	ircCfg.Pass = sauceCfg.Password
	ircCfg.SSL = false

	ircCfg.Me.Nick = sauceCfg.Nickname
	ircCfg.Me.Ident = "SAUCE"
	ircCfg.Me.Name = sauceCfg.Description
	ircCfg.Version = "Saucebot " + saucebotVersion
	ircCfg.QuitMessage = "Shutting down..."

	client := irc.Client(ircCfg)
	quitchan := make(chan bool)

	client.EnableStateTracking()
	client.HandleFunc(irc.CONNECTED, handleConnected)
	client.HandleFunc(irc.PRIVMSG, handleMessage)
	client.HandleFunc(irc.DISCONNECTED, handleDisconnected(quitchan))

	client.HandleFunc(irc.ACTION, handleAll)
	client.HandleFunc(irc.AWAY, handleAll)
	client.HandleFunc(irc.CTCP, handleAll)
	client.HandleFunc(irc.CTCPREPLY, handleAll)
	client.HandleFunc(irc.INVITE, handleAll)
	client.HandleFunc(irc.JOIN, handleAll)
	client.HandleFunc(irc.KICK, handleAll)
	client.HandleFunc(irc.NOTICE, handleAll)
	client.HandleFunc(irc.OPER, handleAll)
	client.HandleFunc(irc.PART, handleAll)
	client.HandleFunc(irc.PASS, handleAll)
	client.HandleFunc(irc.QUIT, handleAll)
	client.HandleFunc(irc.TOPIC, handleAll)
	client.HandleFunc(irc.USER, handleAll)
	client.HandleFunc(irc.VERSION, handleAll)
	client.HandleFunc(irc.VHOST, handleAll)
	client.HandleFunc(irc.WHO, handleAll)
	client.HandleFunc(irc.WHOIS, handleAll)

	log.Printf("IRC bot [%s] connecting to server %s...\n", sauceCfg.Nickname, sauceCfg.Server)
	client.Connect()

	ircBridge = saucebot.NewIRCBridge(client)
	moduleSystem = saucebot.NewModuleSystem(sauceCfg, ircBridge) // implicitly calls .CallEvent("STARTUP", nil)

	go handleInterrupt(client)

	<-quitchan
	moduleSystem = nil
	runtime.GC()
	time.Sleep(time.Second)
}

func handleInterrupt(conn *irc.Conn) {
	sigchan := make(chan os.Signal)
	signal.Notify(sigchan, os.Interrupt)
	<-sigchan

	conn.Quit()
}

func handleConnected(conn *irc.Conn, line *irc.Line) {
	log.Printf("[%s] Connected to IRC server %s\n", sauceCfg.Nickname, sauceCfg.Server)
	moduleSystem.CallEvent(irc.REGISTER, nil)
	time.Sleep(time.Second * 10)
	for chnl, pass := range sauceCfg.Channels {
		conn.Join(chnl, pass)
		log.Printf("[%s] Joined channel %s\n", sauceCfg.Nickname, chnl)
	}
	moduleSystem.CallEvent(irc.CONNECTED, nil)
}

func handleMessage(conn *irc.Conn, line *irc.Line) {
	log.Println(line.Raw)
	event := saucebot.NewEvent(line, conn, sauceCfg.Prefix)

	if event.IsCommand && !event.Local {
		moduleSystem.CallCommand(event.Command, event)
	} else {
		moduleSystem.CallEvent(irc.PRIVMSG, event)
	}
}

func handleDisconnected(quit chan bool) func(*irc.Conn, *irc.Line) {
	return func(conn *irc.Conn, line *irc.Line) {
		moduleSystem.CallEvent(irc.DISCONNECTED, nil)
		quit <- true
		log.Printf(`[%s] Disconnected.`, sauceCfg.Nickname)
	}
}

func handleAll(conn *irc.Conn, line *irc.Line) {
	log.Println(line.Raw)
	event := saucebot.NewEvent(line, conn)
	moduleSystem.CallEvent(line.Cmd, event)
}
